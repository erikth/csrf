<!DOCTYPE HTML>
<html>
<head>
    <title>CSRF example form</title>
</head>
<body>

<?php
require 'vendor/autoload.php';

if (empty($_POST)) { ?>
        <form method="post" action="" accept-charset="UTF-8">
            <?php Csrf::getInstance()->generate(); ?>
            <input type="text" name="field1" />
            <input type="text" name="field2" />
            <button type="submit">Submit</button>
        </form>
<?php
} else {
    echo '<pre>';

    try {
        Csrf::getInstance()->verify();
        // save the form data here...
        print_r($_POST);
    }
    catch (Exception $e) {
        echo $e->getMessage();
    }

    echo '</pre>';
}

?>
</body>
</html>