The Csrf class is used to prevent [Cross Site Request Forgery](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)_Prevention_Cheat_Sheet) attacks. Any form that changes state should use POST (not GET) *and* include a CSRF token.

### How to implement:

A basic form without CSRF protection might look something like this:

```
<form name="form1" method="post" action="/form1/save" accept-charset="UTF-8">
	<input type="input" name="field1" value="" />
	<input type="input" name="field2" value="" />
	<button type="submit" value="save" />
</form>
```

And its endpoint to process the form submission might look like this:

```
<?php
	$formData = $_POST;
	$module = new Vendor_Module();
	try {
		$module->save($formData);
	}
	catch (Exception $e) {
		echo $e->getMessage();
	}
?>
```

To use the Csrf class simply call `generate()` inside your form:

```
<form name="form1" method="post" action="/form1/save" accept-charset-"UTF-8">
	<?php Csrf::getInstance()->generate(); ?>
	<input type="input" name="field1" value="" />
	<input type="input" name="field2" value="" />
	<button type="submit" value="save" />
</form>
```

and then verify the token in the endpoint **before saving the form data**:

```
<?php
    $formData = $_POST;
    $module = new Vendor_Module();
    try {
        Csrf::getInstance()->verify();
        $module->save($formData);
    }
    catch (Exception $e) {
        echo $e->getMessage();
    }
?>
```

### How it works;

The `generate()` method will generate a pipe delimited string consisting of 4 elements:

```
1373583640|k1ps45cdgc2c9v208m483feqg1|e7edf5d5b9cbbd6442ba7e0451c5b73fed1c5d6c51f02b054ca36c4ac5fd57bc|gwJnF5eWwDzwF43Ki8OvXzEReJcJgkor
^          ^                          ^                                                                ^
|          |                          |                                                                |
time       session ID                 sha256 hash of IP and User-Agent                                 32 char random salt
```

This string is then base64-encoded and the token is saved into the user's session with the key name of `Csrf::KEY_NAME`. 

The method then creates a hidden form field with a name of `Csrf::KEY_NAME` and a value of the newly generated token.

This token is submitted with the form. Upon form submission the token must be verified. The verification process consists of several steps:

1. Verify the submitted token matches the token stored in the session
2. Remove the token from session so it cannot be reused
3. Verify the form origin (IP and user-agent hash) and submission origin are the same
4. Verify the time between form generation and submission is less than 5 minutes

If any of these checks fail, an exception will be thrown.

Depending on the situation, you may want to skip one or more checks.

1. The main check (compare form token with session token) cannot be overridden. Because then what's the point of all this? :p
2. A session token may be reused by passing `allowMultiple = true` in the `$args` array. This is especially useful for AJAX requests which may get submitted multiple times.
3. The origin check can be skipped by passing `checkOrigin = false` in the `$args` array to the `verify()` method.
4. The timeout check can be skipped by passing `checkTimeout = false` in the `$args` array to the `verify()` method.

For example:

```
Csrf::getInstance()->verify(array(
	'allowMultiple' => true,
	'checkOrigin'   => false,
	'checkTimeout'  => false
));
```
