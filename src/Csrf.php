<?php
/**
 * Class for preventing Cross Site Request Forgery attacks
 *
 * @package     Csrf
 * @author      Erik TH
 */
class Csrf
{
    /**
     * Name of the session key of the CSRF token
     * @var string
     */
    const KEY_NAME = 'csrf_token';

    /**
     * Token expiration in seconds
     */
    const TIMEOUT = 300; // 5 minutes (60 * 5)

    /**
     * Singleton instance
     * @var object Csrf
     */
    protected static $_instance;

    /**
     * Session instance
     * @var object SecureSession
     */
    protected $_session;

    /**
     * CSRF token
     * @var string
     */
    public $token;

    /**
     * Hidden form field containing CSRF token
     * @var string
     */
    protected $_form;

    /**
     * Constructor
     * 
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $this->_getSession();
    }

    /**
     * Retrieve singleton instance
     * 
     * @return Csrf
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Generate a new CSRF token
     * check for an existing session token and compare it
     * set token in session or reuse an existing token
     * display a hidden form field containing the token value
     *
     * @return void
     */
    public function generate()
    {
        $this->_generateToken()
             ->_compareExistingToken()
             ->_setSessionToken()
             ->_createFormField();

        echo $this->_form;
    }

    /**
     * Verify a CSRF token
     * 
     * @param  array   $args
     *         boolean   allowMultiple true to reuse token (eg: AJAX requests)      default: false
     *         boolean   checkOrigin   true to verify IP and user agent of reqests  default: true
     *         boolean   checkTimeout  true to verify time between reqests          default: true
     * @return boolean                 true if valid token
     */
    public function verify($args = array())
    {
        // parse args
        $allowMultiple = isset($args['allowMultiple']) ? $args['allowMultiple'] : false;
        $checkOrigin = isset($args['checkOrigin']) ? $args['checkOrigin'] : true;
        $checkTimeout =  isset($args['checkTimeout']) ? $args['checkTimeout'] : true;

        // get token from POST
        $token = $_POST[self::KEY_NAME];
        if (empty($token)) {
            $error = 'Missing CSRF form token';
            throw new Exception ($error);
        }

        // get token from session
        $sessionToken = $this->_getSession()->__get(self::KEY_NAME);
        if (null === $sessionToken || empty($sessionToken)) {
            $error = 'Missing CSRF session token';
            throw new Exception ($error);
        }

        // compare the tokens
        if ($token !== $sessionToken) {
            $error = 'Invalid CSRF token!';
            throw new Exception ($error);
        }

        // reuse token?
        if (!$allowMultiple) {
            $this->_unsetSessionToken();
        }

        // if necessary, decode the token for further checks
        if ($checkOrigin || $checkTimeout) {
            $tokenParts = $this->_decodeToken($token);

            // check the origin
            if ($checkOrigin) {
                if ($tokenParts['originHash'] != $this->_hashOrigin()) {
                    $error = 'CSRF origin mismatch';
                    throw new Exception ($error);
                }
            }

            // check the timeout
            if ($checkTimeout) {
                if ($this->_getTime() - $tokenParts['requestTime'] > self::TIMEOUT) {
                    $error = 'CSRF token expired';
                    throw new Exception ($error);
                }
            }
        }

        // passed all checks
        return true;
    }

    /**
     * Generate a CSRF token
     * 
     * @return object Csrf Provides fluent interface
     */
    protected function _generateToken()
    {
        $origin = $this->_hashOrigin();
        $salt = $this->_randomString(32);

        $string = implode('|', array($this->_getTime(), $this->_getSessionId(), $origin, $salt));

        $this->token = base64_encode($string);

        return $this;
    }

    /**
     * Decode a CSRF token into its constituent parts
     * 
     * @param  string $token CSRF token
     * @return array         CSRF token parts
     */
    protected function _decodeToken($token)
    {
        $fields = array('requestTime', 'sessionId', 'originHash', 'salt');

        $parts = explode('|', base64_decode($token));

        return array_combine($fields, $parts);
    }

    /**
     * Compare an existing session token with a newly generated token
     * This will help tie a CSRF token to a user's session and prevent 
     * usability issues ie: opening the same form in multiple tabs
     * 
     * @return object Csrf Provides fluent interface
     */
    protected function _compareExistingToken()
    {
        $existingToken = $this->_getSession()->__get(self::KEY_NAME);
        if (null !== $existingToken) {
            $tokenParts = $this->_decodeToken($existingToken);
            // if session ID, origin hash, and timeout are ok, 
            // reuse the token
            if (
                $tokenParts['sessionId'] == $this->_getSessionId() &&
                $tokenParts['originHash'] == $this->_hashOrigin() &&
                $this->_getTime() - $tokenParts['requestTime'] <= self::TIMEOUT
            ) {
                $this->token = $existingToken;
            }
        }

        return $this;
    }

    /**
     * Create a hidden form field containing the CSRF token
     * 
     * @return object Csrf Provides fluent interface
     */
    protected function _createFormField()
    {
        $this->_form = sprintf('<input type="hidden" name="%s" value="%s" />', 
            self::KEY_NAME, $this->token);

        return $this;
    }

    /**
     * Create a hash (sha256) of client IP and User-Agent string
     * 
     * @return string sha256 hash
     */
    protected function _hashOrigin()
    {
        return hash('sha256', $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);
    }

    /**
     * Create a random string
     * 
     * Do not rely on rand() or mt_rand() for this, as they are cryptographically insecure
     * Length is halved because bin2hex() maps each byte in the input to two bytes in the output 
     * 
     * @param  integer $length
     * @return string
     */
    protected function _randomString($length)
    {
        $bin = openssl_random_pseudo_bytes($length / 2);
        $string = bin2hex($bin);

        return $string;
    }

    /**
     * Get the current session
     * 
     * @return object SecureSession
     */
    protected function _getSession()
    {
        if (null === $this->_session) {
            $this->_session = new SecureSession;
        }
        return $this->_session;
    }

    /**
     * Set the session instance
     * 
     * @param  object $session
     */
    public function setSession($session)
    {
        $this->_session = $session;
    }

    /**
     * Set the CSRF token in session
     * 
     * @return object Csrf Provides fluent interface
     */
    protected function _setSessionToken()
    {
        $this->_getSession()->__set(self::KEY_NAME, $this->token);

        return $this;
    }

    /**
     * Unset the CSRF token and key from session
     * 
     * @return void
     */
    protected function _unsetSessionToken()
    {
        $this->_getSession()->__unset(self::KEY_NAME);
    }

    /**
     * Get current time
     * 
     * @return int
     */
    protected function _getTime()
    {
        return time();
    }

    /**
     * Get session ID
     * 
     * @return string 
     */
    protected function _getSessionId()
    {
        return session_id();
    }

}