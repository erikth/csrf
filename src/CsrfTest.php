<?php
/**
 * Csrf Class unit tests
 * 
 * @package     Csrf
 * @author      Erik TH
 */
class CsrfTest extends PHPUnit_Framework_TestCase
{
    protected function setUp() 
    {        
        // buffer output because generate() echos the form
        ob_start();
        // start the session with errors suppressed because the session uses cookies
        // and test will error with cookies sent in the headers
        @session_start();

        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $_SERVER['HTTP_USER_AGENT'] = 'whatever';
    }

    protected function tearDown()
    {
        // clean the output buffer
        ob_end_clean();
    }

    /**
     * @group   Csrf
     */
    public function testInstance()
    {
        $csrf = new Csrf();

        $this->assertInstanceOf('Csrf', $csrf);
    }

    /**
     * @group   Csrf
     */
    public function testSingletonInstance()
    {
        $csrf = Csrf::getInstance();

        $this->assertInstanceOf('Csrf', $csrf);
    }

    /**
     * @group   Csrf
     */
    public function testGenerate()
    {
        $csrf = new Csrf;

        $csrf->generate();

        $form = ob_get_contents();
        
        // get the input attributes
        $dom = new DOMDocument;
        @$dom->loadHtml($form);
        $input = $dom->getElementsByTagName('input');
        $type = $input->item(0)->getAttribute('type');
        $name = $input->item(0)->getAttribute('name');
        $value = $input->item(0)->getAttribute('value');

        $this->assertInternalType('string', $form);
        $this->assertEquals($type, 'hidden');
        $this->assertEquals($name, Csrf::KEY_NAME);
        $this->assertEquals($value, $csrf->token);
    }

    /**
     * @group   Csrf
     */
    public function testGenerateWithExistingValidToken()
    {
        $csrf = new Csrf;

        $csrf->generate();
        $firstToken = $csrf->token;

        $_POST[Csrf::KEY_NAME] = $csrf->token;
        $csrf->verify(array('allowMultiple' => true));

        $csrf->generate();
        $secondToken = $csrf->token;

        $this->assertEquals($firstToken, $secondToken);
    }

    /**
     * @group   Csrf
     */
    public function testGenerateWithExistingInvalidToken()
    {
        $csrf = new Csrf();

        $csrf->generate();
        $firstToken = $csrf->token;

        $_POST[Csrf::KEY_NAME] = $csrf->token;
        // verify will invalidate the first token
        $csrf->verify();

        $csrf->generate();
        $secondToken = $csrf->token;

        $this->assertNotEquals($firstToken, $secondToken);
    }

    /**
     * @group   Csrf
     */
    public function testVerify()
    {
        $csrf = new Csrf();
        
        $csrf->generate();

        $_POST[Csrf::KEY_NAME] = $csrf->token;

        $result = $csrf->verify();

        $this->assertEquals(true, $result);
    }

    /**
     * @group   Csrf
     */
    public function testVerifyNoFormToken()
    {
        $csrf = new Csrf();

        $_POST[Csrf::KEY_NAME] = '';

        $this->setExpectedException('Exception', 
            'Missing CSRF form token'
        );

        $csrf->verify();
    }

    /**
     * @group   Csrf
     */
    public function testVerifyNoSessionToken()
    {
        $csrf = new Csrf();

        $_POST[Csrf::KEY_NAME] = 'whatever';

        $this->setExpectedException('Exception', 
            'Missing CSRF session token'
        );

        $csrf->verify();
    }

    /**
     * @group Csrf
     */
    public function testVerifyInvalidToken()
    {
        $csrf = new Csrf();

        $csrf->generate();

        $_POST[Csrf::KEY_NAME] = 'whatever';

        $this->setExpectedException('Exception', 
            'Invalid CSRF token!'
        );

        $csrf->verify();
    }

    /**
     * @group   Csrf
     */
    public function testVerifyBadOrigin()
    {
        $csrf = $this->getMock('Csrf', array(
            '_hashOrigin'
        ));

        $goodHash = hash('sha256', $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);
        $badHash = hash('sha256', 'crap');

        $csrf->expects($this->at(0))
             ->method('_hashOrigin')
             ->will($this->returnValue($goodHash));
        
        $csrf->expects($this->any())
             ->method('_hashOrigin')
             ->will($this->returnValue($badHash));


        $csrf->generate();

        $_POST[Csrf::KEY_NAME] = $csrf->token;

        $this->setExpectedException('Exception', 
            'CSRF origin mismatch'
        );

        $csrf->verify();
    }

    /**
     * @group   Csrf
     */
    public function testVerifyNoOriginCheck()
    {
        $csrf = $this->getMock('Csrf', array(
            '_hashOrigin'
        ));

        $goodHash = hash('sha256', $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);
        $badHash = hash('sha256', 'crap');

        $csrf->expects($this->at(0))
             ->method('_hashOrigin')
             ->will($this->returnValue($goodHash));
        
        $csrf->expects($this->any())
             ->method('_hashOrigin')
             ->will($this->returnValue($badHash));

        $csrf->generate();

        $_POST[Csrf::KEY_NAME] = $csrf->token;

        $result = $csrf->verify(array('checkOrigin' => false));

        $this->assertEquals($result, true);
    }

    /**
     * @group   Csrf
     */
    public function testVerifyExpiredToken()
    {
        $csrf = $this->getMock('Csrf', array(
            '_getTime'
        ));

        $time = time();
        $newTime = time() + Csrf::TIMEOUT + 1;

        $csrf->expects($this->at(0))
             ->method('_getTime')
             ->will($this->returnValue($time));

        $csrf->expects($this->any())
             ->method('_getTime')
             ->will($this->returnValue($newTime));

        $csrf->generate();

        $_POST[Csrf::KEY_NAME] = $csrf->token;

        $this->setExpectedException('Exception', 
            'CSRF token expired'
        );

        $csrf->verify();
    }

    /**
     * @group   Csrf
     */
    public function testVerifyNoExpiryCheck()
    {
        $csrf = $this->getMock('Csrf', array(
            '_getTime'
        ));

        $time = time();
        $newTime = time() + Csrf::TIMEOUT + 1;

        $csrf->expects($this->at(0))
             ->method('_getTime')
             ->will($this->returnValue($time));

        $csrf->expects($this->any())
             ->method('_getTime')
             ->will($this->returnValue($newTime));

        $csrf->generate();

        $_POST[Csrf::KEY_NAME] = $csrf->token;

        $result = $csrf->verify(array('checkTimeout' => false));

        $this->assertEquals($result, true);
    }

}